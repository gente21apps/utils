const fs = require('fs');


var file = process.argv[2];
console.log('Generating heads from: '+file+'\n\n');
//return false;
var file = process.argv[2];

function camelToPrhase(text){
	var result = text.replace( /([A-Z])/g, " $1" );
	return result.charAt(0).toUpperCase() + result.slice(1);
}

fs.readFile('definitiosFormatedTable', 'utf8', function(err, data) {

	var columnsOne = data;
	var allColumns = columnsOne.split("\n");
	var output = [];

	for(let column of allColumns){
		let inner = column.split(' ');
		let item = {};
		if(inner[0].trim() == 'id'){
			item = {
				key: 'id',
				name: 'ID',
				width: 80,
				sortable: true,
			}
		} else if(inner[1].includes('tinyint')){
			item = {
				key: inner[0],
		        name: camelToPrhase(inner[0]),
		        sortable: true,
		        formatter: 'StatusFormatter',
		        width: 110,
			}
		} else {
			item = {
				key: inner[0],
		        name: camelToPrhase(inner[0]),
		        sortable: true,
			}
		}
		output.push(item);
	}

	console.log(output);

});