const fs = require('fs');


var file = process.argv[2];
console.log('Generating heads from: '+file+'\n\n');
//return false;
var file = process.argv[2];

function camelToPrhase(text){
	var result = text.replace( /([A-Z])/g, " $1" );
	return result.charAt(0).toUpperCase() + result.slice(1);
}

fs.readFile('definitiosFormatedTable', 'utf8', function(err, data) {

	var columnsOne = data;
	var allColumns = columnsOne.split("\n");
	var out = '';

	for(let column of allColumns){
		let inner = column.split(' ');
		if(inner[1].includes('int') || inner[1].includes('tinyint')){
			out += inner[0]+': 0,\n';
		} else if(inner[1].includes('varchar')){
			out += inner[0]+": '',\n";
		} else if(inner[1].includes('timestamp') || inner[1].includes('datetime')){
			out += inner[0]+": moment().unix() * 1000,\n";
		} else if(inner[1].includes('float')){
			out += inner[0]+': 0,\n';
		} else {
			console.log('>>type NOT RECOGNIZED!!!! '+inner[1]);
		}
	}

	console.log(out);

});