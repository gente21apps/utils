var childProcess = require('child_process');

var file = process.argv[2];
var singular = process.argv[3];
console.log('STARTING: '+file+' singular? '+singular+' \n\n');

function runScript(scriptPath, file, singular, callback) {
    // keep track of whether callback has been invoked to prevent multiple invocations
    var invoked = false;
    var name = file
    var singular = singular;
    var args = [name, singular];
    var process = childProcess.fork(scriptPath, args);

    // listen for errors as they may prevent the exit event from firing
    process.on('error', function (err) {
        if (invoked) return;
        invoked = true;
        callback(err);
    });

    // execute the callback once the process has finished running
    process.on('exit', function (code) {
        if (invoked) return;
        invoked = true;
        var err = code === 0 ? null : new Error('exit code ' + code);
        callback(err);
    });
}

runScript('./createModel.js', file, singular, function (err) {
    if (err) {
        throw err;
    } else {
        console.log('>> Model created');
    }
});

runScript('./createController.js', file, singular, function (err) {
    if (err) {
        throw err;
    } else {
        console.log('>> Controller created');
    }
});

runScript('./createDao.js', file, singular, function (err) {
    if (err) {
        throw err;
    } else {
        console.log('>> Dao created');
    }
});