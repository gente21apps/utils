const fs = require('fs');


var file = process.argv[2];
console.log('Generating heads from: '+file+'\n\n');
//return false;
var file = process.argv[2];

function camelToPrhase(text){
	var result = text.replace( /([A-Z])/g, " $1" );
	return result.charAt(0).toUpperCase() + result.slice(1);
}

fs.readFile('definitiosFormatedTable', 'utf8', function(err, data) {

	var columnsOne = data;
	var allColumns = columnsOne.split("\n");
	var out = '';

	for(let column of allColumns){
		let inner = column.split(' ');

		if(inner[0] == 'id'){
			continue;
		}
		if(inner[1].includes('tinyint')){
			out += '<div className="form__form-group">'
                      +'<TCheckBox onChange={this.handleInputChange} name="'+inner[0]+'"'
                        +'value={!!'+inner[0]+'} label="'+camelToPrhase(inner[0])+'"'
                      +'/>'
                    +'</div>\n\n';
		} else if(inner[1].includes('int') || inner[1].includes('float')){
			out += '<div className="form__form-group">\n'
                    +'<span className="form__form-group-label">'+camelToPrhase(inner[0])+'</span>\n'
                    +'<div className="form__form-group-field">\n'
                      +'<input name="'+inner[0]+'" type={\'number\'} value={this.state.'+inner[0]+'} onChange={this.handleInputChange} />\n'
                    +'</div>\n'
                  	+'</div>\n\n';
		} else if (inner[1].includes('tinyint')){
			out += '<div className="form__form-group">\n'
                    +'<TCheckBox onChange={this.handleInputChange} name="'+inner[0]+'" value={this.state.'+inner[0]+' ? true : false} label="'+camelToPrhase(inner[0])+'" />\n'
                  +'</div>\n\n';
		} else if(inner[1].includes('varchar')){
			out += '<div className="form__form-group">\n'
                    +'<span className="form__form-group-label">'+camelToPrhase(inner[0])+'</span>\n'
                    +'<div className="form__form-group-field">\n'
                      +'<input name="'+inner[0]+'" type={\'text\'} value={this.state.'+inner[0]+'} onChange={this.handleInputChange} />\n'
                    +'</div>\n'
                    +'</div>\n\n';
		} else if(inner[1].includes('timestamp') || inner[1].includes('datetime')){
			out += '<div className="form__form-group">\n'
                    +'<span className="form__form-group-label">'+camelToPrhase(inner[0])+'</span>\n'
                    +'<div className="form__form-group-field">\n'
                      +'<input name="'+inner[0]+'" type={\'text\'} value={moment(this.state.'+inner[0]+').format()} onChange={this.handleInputChange} disabled />\n'
                    +'</div>\n'
                  +'</div>\n\n';
		} else {
			console.log('>>type NOT RECOGNIZED!!!! '+inner[1]);
		}
	}

	console.log(out);

});