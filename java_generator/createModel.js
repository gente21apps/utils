
const fs = require('fs');


var file = process.argv[2];
console.log('STARTING: '+file+'\n\n');
//return false;
var file = process.argv[2];

fs.readFile('definitions', 'utf8', function(err, data) {
  
	var columnsOne = data;
	var allColumns = columnsOne.split("\n");
	var columns = [];

	for(let column of allColumns){
		// console.log('>>>ES: ', column);
		let inner = column.split(' ');
		type = '';
		if(inner[1].includes('int') || inner[1].includes('tinyint')){
			type = 'Integer'
		} else if(inner[1].includes('varchar') || inner[1].includes('string')){
			type = 'String'
		} else if(inner[1].includes('collection')){
			type = 'List<String>'
		} else if(inner[1].includes('timestamp') || inner[1].includes('datetime')){
			type = 'Timestamp'
		} else if(inner[1].includes('float') || inner[1].includes('number')){
			type = 'Double'
		} else {
			console.log('>>TYPE NOT RECOGNIZED!!!! '+inner[1]);
		}
		let item = {
			name: inner[0],
			type: type
		}
		columns.push(item);
	}

	function capitalizeFirstLetter(string) {
	    return string.charAt(0).toUpperCase() + string.slice(1);
	}

	var modelClass = "package net.trelar.orion.model;\n\n"
		+"import javax.persistence.*;\n"
		+"import java.sql.Timestamp;\n"
		+"import java.util.Objects;\n\n"
		+"@Entity\n"
		+"@Table(name = \""+file+"s\", schema = \"orion\")\n"
		+"public class "+file+" {\n\n";

	//private members
	for (let column of columns){
		let model = "private "+column.type+" "+column.name+";\n";
		modelClass += model;
	}

	modelClass += "\n\n";

	//setters / getters
	for (let column of columns){
		
		if(column.name == 'id'){
			let model = "@Id\n"
			    +"@Column(name = \"id\")\n"
			    +"@GeneratedValue(strategy=GenerationType.IDENTITY)\n"
			    +"public Integer getId() {\n"
			    +"    return id;\n"
			    +"}\n"
			    +"public void setId(Integer id) {\n"
			    +"    this.id = id;\n"
			    +"}\n\n";
			modelClass += model;
		} else {
			let model = "@Basic\n"
			    +"@Column(name = \""+column.name+"\")\n"
			    +"public "+column.type+" get"+capitalizeFirstLetter(column.name)+"() {\n"
			    +"    return "+column.name+";\n"
			    +"}\n"
			    +"public void set"+capitalizeFirstLetter(column.name)+"("+column.type+" "+column.name+") {\n"
			    +"    this."+column.name+" = "+column.name+";\n"
			    +"}\n\n";
			modelClass += model;
		}			
	}

	modelClass += "\n";

	//hashChode
	modelClass += "@Override\n"
		+"public int hashCode() {\n"
		+"    return Objects.hash(\n";
	columns.forEach(function(column, idx, array){
		if (idx === array.length - 1){ 
			modelClass += "        "+column.name+"\n" 
		} else {
			modelClass += "        "+column.name+",\n" 
		}
	});
	modelClass += "    );\n"
		+"}\n\n"
		+"}\n\n";

	//console.log(modelClass);

	fs.writeFile(file+'.java', modelClass, function(err) {
	    if(err) {
	        return console.log(err);
	    }

	    //console.log("The file "+file+" was saved!");
	}); 

});

	

//