package net.trelar.orion.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "chidos", schema = "orion")
public class chido {

private Integer id;
private Integer bookingEquipmentId;
private Timestamp recordedAt;
private Integer createdBy;
private Timestamp createdOn;
private Integer modifiedBy;
private Timestamp modifiedOn;
private Integer isArchived;
private Integer location;


@Id
@Column(name = "id")
@GeneratedValue(strategy=GenerationType.IDENTITY)
public Integer getId() {
    return id;
}
public void setId(Integer id) {
    this.id = id;
}

@Basic
@Column(name = "bookingEquipmentId")
public Integer getBookingEquipmentId() {
    return bookingEquipmentId;
}
public void setBookingEquipmentId(Integer bookingEquipmentId) {
    this.bookingEquipmentId = bookingEquipmentId;
}

@Basic
@Column(name = "recordedAt")
public Timestamp getRecordedAt() {
    return recordedAt;
}
public void setRecordedAt(Timestamp recordedAt) {
    this.recordedAt = recordedAt;
}

@Basic
@Column(name = "createdBy")
public Integer getCreatedBy() {
    return createdBy;
}
public void setCreatedBy(Integer createdBy) {
    this.createdBy = createdBy;
}

@Basic
@Column(name = "createdOn")
public Timestamp getCreatedOn() {
    return createdOn;
}
public void setCreatedOn(Timestamp createdOn) {
    this.createdOn = createdOn;
}

@Basic
@Column(name = "modifiedBy")
public Integer getModifiedBy() {
    return modifiedBy;
}
public void setModifiedBy(Integer modifiedBy) {
    this.modifiedBy = modifiedBy;
}

@Basic
@Column(name = "modifiedOn")
public Timestamp getModifiedOn() {
    return modifiedOn;
}
public void setModifiedOn(Timestamp modifiedOn) {
    this.modifiedOn = modifiedOn;
}

@Basic
@Column(name = "isArchived")
public Integer getIsArchived() {
    return isArchived;
}
public void setIsArchived(Integer isArchived) {
    this.isArchived = isArchived;
}

@Basic
@Column(name = "location")
public Integer getLocation() {
    return location;
}
public void setLocation(Integer location) {
    this.location = location;
}


@Override
public int hashCode() {
    return Objects.hash(
        id,
        bookingEquipmentId,
        recordedAt,
        createdBy,
        createdOn,
        modifiedBy,
        modifiedOn,
        isArchived,
        location
    );
}

}

