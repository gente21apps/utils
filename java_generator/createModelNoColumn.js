
const fs = require('fs');

// NOTE:
// this is for generating models for Hyperwallet
// please copy and modify if needed :D

// use:
// node createModelNoColumn.js HWPayment HyperwalletPayment

var newClazz = process.argv[2];
var clazz = process.argv[3];

fs.readFile('definitionsnocolumn', 'utf8', function(err, data) {
  
	var columnsOne = data;
	var allColumns = columnsOne.split("\n");
	var columns = [];

	for(let column of allColumns){
		let inner = column.split(' ');
		let item = {
			name: inner[2].replace(";\r", ""),
			type: inner[1]
		}
		columns.push(item);
	}

	/*	
	console.log(allColumns);
	console.log("-----------------------------------");
	console.log(columns);
	return false;
	*/

	function capitalizeFirstLetter(string) {
	    return string.charAt(0).toUpperCase() + string.slice(1);
	}

	var modelClass = "package net.trelar.orion.model.payment;\n\n"
		+"import javax.persistence.*;\n"
		+"import com.hyperwallet.clientsdk.model."+clazz+";\n"
		+"import java.sql.Timestamp;\n"
		+"import java.util.Date;\n"
		+"import java.util.Objects;\n\n"
		+"@Entity\n"
		+"public class "+newClazz+" {\n\n";

	//private members
	for (let column of columns){
		// private HyperwalletUser.Status status;
		let model = '';
		if(column.name == 'id'){
			model = "private "+column.type+" "+column.name+";\n";
		} else {
			model = "private "+column.type+" "+column.name+";\n";
		}		
		modelClass += model;
	}

	modelClass += "\n\n";

	//setters / getters
	for (let column of columns){
		if(column.name == 'id'){
			let model = "@Id\n"
			    // +"@Column(name = \"id\")\n"
			    +"@GeneratedValue(strategy=GenerationType.IDENTITY)\n"
			    +"public Integer getId() {\n"
			    +"    return id;\n"
			    +"}\n"
			    +"public void setId(Integer id) {\n"
			    +"    this.id = id;\n"
			    +"}\n\n";
			modelClass += model;
		} else {
			let model = "@Basic\n"
			    // +"@Column(name = \""+column.name+"\")\n"
			    +"public "+capitalizeFirstLetter(column.type)+" get"+capitalizeFirstLetter(column.name)+"() {\n"
			    +"    return "+column.name+";\n"
			    +"}\n"
			    +"public void set"+capitalizeFirstLetter(column.name)+"("+capitalizeFirstLetter(column.type)+" "+column.name+") {\n"
			    +"    this."+column.name+" = "+column.name+";\n"
			    +"}\n\n";
			modelClass += model;
		}
			
	}

	modelClass += "\n";

	//hashChode
	modelClass += "@Override\n"
		+"public int hashCode() {\n"
		+"    return Objects.hash(\n";
	columns.forEach(function(column, idx, array){
		if (idx === array.length - 1){ 
			modelClass += "        "+column.name+"\n" 
		} else {
			modelClass += "        "+column.name+",\n" 
		}
	});
	modelClass += "    );\n"
		+"}\n\n"
		+"}\n\n";

	console.log(modelClass);

	//for now do not print, just output to screen
	return false;

	/*
	fs.writeFile(file+'.java', modelClass, function(err) {
	    if(err) {
	        return console.log(err);
	    }
	});
	*/

});
